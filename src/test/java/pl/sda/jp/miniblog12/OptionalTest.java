package pl.sda.jp.miniblog12;

import org.junit.Test;
import pl.sda.jp.miniblog12.entity.User;

import java.util.Optional;

public class OptionalTest {

    @Test
    public void orElse_vs_orElseGet(){
        User user = new User();
        user.setFirstName("Jarek");

        User nullUser = null;

        System.out.println("--- 1 ---");
        Optional.ofNullable(nullUser).orElse(new User());
        System.out.println("--- 2 ---");
        Optional.ofNullable(user).orElse(new User());
        System.out.println("--- 3 ---");
        Optional.ofNullable(nullUser).orElseGet(() -> new User());
        System.out.println("--- 4 ---");
        Optional.ofNullable(user).orElseGet(() -> new User());

        String s = Optional.ofNullable(user)
                .map(u -> u.getFirstName())

                .orElseGet(()->"UNKNOWN");


    }
}
