package pl.sda.jp.miniblog12;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.sda.jp.miniblog12.entity.Post;
import pl.sda.jp.miniblog12.form.NewPostForm;
import pl.sda.jp.miniblog12.service.PostService;
import pl.sda.jp.miniblog12.service.UserContextService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class PostController {

    private PostService postService;
    private UserContextService userContextService;

    @Autowired
    public PostController(PostService postService, UserContextService userContextService) {
        this.postService = postService;
        this.userContextService = userContextService;
    }

    @GetMapping("/post/add")
    public String showAddNewPostForm(Model model){
        model.addAttribute("newPostForm", new NewPostForm());
        return "post/addNewPostForm";
    }

    @PostMapping("/post/add")
    public String handleNewPostForm(
            @ModelAttribute @Valid NewPostForm newPostForm, BindingResult bindingResult
    ){
        if(bindingResult.hasErrors()){
            return "post/addNewPostForm";
        }

        postService.addNewPost(newPostForm);

        return "redirect:/";
    }

    @GetMapping("/post")
//    public String showSinglePost(@RequestParam Long postId){
    public String showSinglePost(@RequestParam String postId, Model model){

        return prepareSinglePost(postId, model);
    }

    @GetMapping("/post/{postName},{postId}")
//    @GetMapping("/post/{postId}")
    public String showSinglePostByPath(@PathVariable String postId, Model model){

        return prepareSinglePost(postId, model);
    }

    @GetMapping("/posts")
    public String showAllPosts(Model model){
        List<Post> allPosts = postService.getAllPosts();

        model.addAttribute("posts", allPosts);

        return "post/showPosts";
    }


    @PostMapping("/post/{id}/comment/add")
    public String handleNewCommentForm(
            @PathVariable String id,
            @RequestParam String commentBody,
            @RequestParam String postId
    ){
        postService.addNewComment(Long.parseLong(id), userContextService.getLoggedAs(), commentBody);
        return "redirect:/post/," + postId;
    }




    private String prepareSinglePost(@RequestParam String postId, Model model) {
        Long postIdLong;
        try {
            postIdLong = Long.parseLong(postId);
        } catch (NumberFormatException e) {
            return "post/postNotFound";
        }

        boolean showCommentForm = userContextService.getLoggedAs() != null
                && userContextService.hasRole("ROLE_USER");

        model.addAttribute("showCommentForm", showCommentForm);

        Optional<Post> postOptional = postService.getSinglePost(postIdLong);
        if (postOptional.isPresent() == false) {
            return "post/postNotFound";
        }

        model.addAttribute("post", postOptional.get());
        return "post/showSinglePost";
    }
}
