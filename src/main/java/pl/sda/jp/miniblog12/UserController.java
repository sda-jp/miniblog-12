package pl.sda.jp.miniblog12;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.sda.jp.miniblog12.form.UserRegisterForm;
import pl.sda.jp.miniblog12.service.UserService;
import pl.sda.jp.miniblog12.service.UserSessionService;

import javax.validation.Valid;

@Controller
public class UserController {

    private UserService userService;
    private UserSessionService userSessionService;

    @Autowired
    public UserController(UserService userService, UserSessionService userSessionService) {
        this.userService = userService;
        this.userSessionService = userSessionService;
    }

    @GetMapping("/register")
    public String showRegisterForm(Model model){
        model.addAttribute("userRegisterForm",new UserRegisterForm());
        return "user/registerForm";
    }

    @PostMapping("/register")
    public String handleRegisterForm(
            @ModelAttribute @Valid UserRegisterForm userRegisterForm,
            BindingResult bindingResult
            ){

        if(bindingResult.hasErrors()){
            return "user/registerForm";
        }
        //System.out.println(firstName + ", " + lastName + ", " + email + ", " + password);
        //System.out.println(userRegisterForm);

        userService.registerUser(userRegisterForm);

        return "redirect:/home";
    }


    @GetMapping("/login-by-spring")
    public String showLoginFormBySpringSecurity(){
        return "user/loginFormBySpring";
    }

    @GetMapping("/loginForm")
    public String showLoginForm(){
        return "user/loginForm";
    }

    @PostMapping("/loginForm")
    public String handleLoginForm(@RequestParam String username,
                                  @RequestParam String password){

        boolean userLogged = userSessionService.loginUser(username, password);

        if(!userLogged){
            return "user/loginForm";
        }
        return "redirect:/";
    }

    @GetMapping("/logoutForm")
    public String logoutForm(){
        userSessionService.logout();
        return "user/logoutForm";
    }
}
