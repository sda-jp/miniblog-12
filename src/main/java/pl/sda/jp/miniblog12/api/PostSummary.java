package pl.sda.jp.miniblog12.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
public class PostSummary {
    private final Long id;
    private final String title;
}
